var ids = [
    {
        "name" : "Liviu Ralea",
        "id"   : "lralea"
    },
    {
        "name" : "Gabriela Mateescu",
        "id"   : "gmateescu"
    },
    {
      "name" : "Petre Fall",
      "id"   : "pfall"
    },
    {
        "name" : "Tatiana Moise",
        "id"   : "tmoise"
    },
    {
        "name" : "Ioana Fery",
        "id"   : "ifery"
    },
    {
        "name" : "",
        "id"   : "iblackandfield"
    },
    {
	    "name" : "Mitran Mihai",
	    "id" : "mmihai"
    }

];

var itemsProcessed = 0;
var c  = 0;
var lim = 3;
$(document).ready(function(){
// note : onAtPage
  class Worms extends Paged.Handler {

		constructor(chunker, polisher, caller) {
			super(chunker, polisher, caller);
		}

		afterPageLayout(pageElement, page, breakToken) {


          var classList    = page.element.classList;
          var page_type    = classList[classList.length-1];
          var side         = page_type.split("_")[1];
          var rightsidebar = $("#page-"+c).find(".pagedjs_margin-right");
          var leftsidebar  = $("#page-"+c).find(".pagedjs_margin-left");

          // console.log(side);
          if(side != "right"){
              if( c > lim){
                var paju_arru = []
                for( var i = lim; i < c; i++){
                  paju_arru.push(i);
                }
                $(rightsidebar[0]).append("<div class='paju'>"+paju_arru.join('<br/>')+"</div>");

              }

          }else{

          }
          c++;

        }

	}
	Paged.registerHandlers(Worms);


    ids.forEach(function(e){
        itemsProcessed++;
        $.ajax({
    		context: this,
    		dataType : "html",
            async: false,
    		url : "http://51.254.121.46:9001/p/wh_list_"+e.id+"/export/html",
    		success : function(results) {
                // console.log(results)

                $("body").append("<section class='intro'>"+results+"<div class='reslisible'>"+results+"</diV></section>");

            },
            error: function(){
                console.log("error");
            }
        });
        $.ajax({
    		context: this,
    		dataType : "html",
            async: false,
    		url : "http://51.254.121.46:9001/p/wh_pad_"+e.id+"/export/html",
    		success : function(results) {
                // console.log(results)

                $("body").append("<section class='content'>"+results+"</section>");
                $("meta").remove();
                $("style").remove();
                $("title").remove();
            },
            error: function(){
                console.log("error");
            }
        });
        //console.log(itemsProcessed);
        if(itemsProcessed === ids.length) {
            console.log("done");
            var contenu = $("body .temp").html();
            $("body").append("<div id='contenu'>"+contenu+"</div>");
            $("body .temp").remove();
            window.PagedPolyfill.preview();
        }
    });
//window.PagedPolyfill.preview();

});
