var login = window.location.search;
var words;
var viruses = [];
var login_allowed = ["mmihai","lralea","gmateescu","slungu","tmoise","mghiu","b997","ifery","moprea","iblackandfield","rfay","apantazica","apatatics","cbolozan","tstefan","pfall","bjm"];

$(document).ready(function(){
    virusPick();
    var res = login.replace("?", "");
    if( res != "" && res != null && res != undefined && login_allowed.includes(res)){

        $('#right').pad({
            'host'             : 'http://51.254.121.46:9001',
            'padId'            : 'wh_pad_'+res,
            'showChat'         : false,
            'showControls'     : true
        });
        $('#container').pad({
            'host'             : 'http://51.254.121.46:9001',
            'padId'            : 'wh_list_'+res,
            'showChat'         : false,
            'showControls'     : false,
            'useMonospaceFont' : true
        });
        fetch(res);

    }else{
        alert("wrong login !\n(눈_눈)")
    }

    var draggable = $('#topbar'); //element
    draggable.on('mousedown', function(e){
    	var dr = $(this).parent().addClass("drag").css("cursor","move");
    	height = dr.outerHeight();
    	width = dr.outerWidth();
    	ypos = dr.offset().top + height - e.pageY,
    	xpos = dr.offset().left + width - e.pageX;
    	$(document.body).on('mousemove', function(e){
    		var itop = e.pageY + ypos - height;
    		var ileft = e.pageX + xpos - width;
    		if(dr.hasClass("drag")){
    			dr.offset({top: itop,left: ileft});
    		}
    	}).on('mouseup', function(e){
    			dr.removeClass("drag");
    	});
    });

    $(".button").click(function(){
        $('#list').toggleClass("hide");
    });
    $(".show_info").click(function(){
        $('.virus_box').toggleClass("flip_me");
    });


    $(".sh_p").click(function(){
        fetch(res);
    })
    $(".sh_v").click(function(){

        virusPick();
        if ($('.virus_box').hasClass("flip_me") == true) {
            $('.virus_box').toggleClass("flip_me");
        }

    });



});
function virusPick(){
    $.ajax({
		context: this,
		dataType : "html",
		url : "http://51.254.121.46:9001/p/wiki_bonjourmonde.Liste-Virus/export/txt",
		success : function(results) {

            var fs = results.split("---");
            fs.forEach(function(e){
                var elem = e.split("--");
                if(elem[0] != ""){
                    var virus = {
                        'name' : elem[0],
                        'info' : elem[1]
                    }
                    viruses.push(virus);
                }
            });
            viruses = shuffle(viruses);
            $(".side_one h3").html(viruses[0]["name"]);
            $(".side_one h2").html(viruses[0]["name"]);

            $(".side_two p").html(viruses[0]["info"]);

        }
    });
}
function fetch(name){
    $.ajax({
		context: this,
		dataType : "html",
		url : "http://51.254.121.46:9001/p/wh_list_"+name+"/export/txt",
		success : function(results) {

            words = results.split("\n");
            var index = words.indexOf("");
            if (index !== -1) words.splice(index, 1);
            words = shuffle(words);
            $(".word h3").html(words[0]);
            $(".word h2").html(words[0]);

        }
    });
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

/*pad js*/
/*
'host'             : 'http://beta.etherpad.org', // the host and port of the Etherpad instance, by default the foundation will host your pads for you
'baseUrl'          : '/p/', // The base URL of the pads
'showControls'     : false, // If you want to show controls IE bold, italic, etc.
'showChat'         : false, // If you want to show the chat button or not
'showLineNumbers'  : false, // If you want to show the line numbers or not
'userName'         : 'unnamed', // The username you want to pass to the pad
'useMonospaceFont' : false, // Use monospaced fonts
'noColors'         : false, // Disable background colors on author text
'userColor'        : false, // The background color of this authors text in hex format IE #000
'hideQRCode'       : false, // Hide QR code
'alwaysShowChat'   : false, // Always show the chat on the UI
'width'            : 100, // The width of the embedded IFrame
'height'           : 100,  // The height of the embedded IFrame
'border'           : 0,    // The width of the border (make sure to append px to a numerical value)
'borderStyle'      : 'solid', // The CSS style of the border	[none, dotted, dashed, solid, double, groove, ridge, inset, outset]
'plugins'          : {}, // The options related to the plugins, not to the basic Etherpad configuration
'rtl'              : false // Show text from right to left
*/
